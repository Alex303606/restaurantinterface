import React,{Component} from 'react';
import './Restaurant.css';
import Menu from "../../components/Menu/Menu";
import Card from "../../components/Card/Card";

class Restaurant extends Component {
	render(){
		return (
			<div className="Restaurant">
				<Menu/>
				<Card/>
			</div>
		)
	}
}

export default Restaurant;