import React, {Component} from 'react';
import {connect} from 'react-redux';
import './Order.css'
import {placeOrder} from "../../store/actions/OrderActions";
import Spinner from "../UI/Spinner/Spinner";
import {resetOrderList} from "../../store/actions/menuActions";

class Order extends Component {
	
	state = {
		name: '',
		email: '',
		street: ''
	};
	
	valueChanged = (e) => {
		const name = e.target.name;
		this.setState({[name]: e.target.value});
	};
	
	orderHandler = (e) => {
		e.preventDefault();
		const dishes = {};
		Object.keys(this.props.dishes).map(key => {
			if(this.props.dishes[key].amount > 0) {
				return dishes[key] = this.props.dishes[key].amount;
			} else {return null;}
		});
		const order = {
			dishes: dishes,
			customer: {
				name: this.state.name,
				email: this.state.email,
				street: this.state.street
			},
			total: this.props.totalPrice
		};
		this.props.onPlaceOrder(order);
		this.props.resetOrderList();
	};
	
	render() {
		const form = (
			<div className="order">
				<h4>Enter your Contact Data</h4>
				<form onSubmit={this.orderHandler}>
					<input required onChange={(e) => this.valueChanged(e)} value={this.state.name} className="Input"
					       type="text"
					       name="name"
					       placeholder="Your Name"/>
					<input required onChange={(e) => this.valueChanged(e)} value={this.state.email} className="Input"
					       type="email"
					       name="email"
					       placeholder="Your Mail"/>
					<input required onChange={(e) => this.valueChanged(e)} value={this.state.street} className="Input"
					       type="text"
					       name="street"
					       placeholder="Street"/>
					<button type="submit">Create order</button>
				</form>
			</div>
		);
		return this.props.loading ? <Spinner/> : form;
	}
}

const mapStateToProps = state => {
	return {
		dishes: state.dh.dishes,
		loading: state.cd.loading,
		totalPrice: state.dh.totalPrice
	}
};

const mapDispatchToProps = dispatch => {
	return {
		onPlaceOrder: (order) => dispatch(placeOrder(order)),
		resetOrderList: () => dispatch(resetOrderList())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Order);