import React, {Component} from 'react';
import './Menu.css';
import {connect} from 'react-redux';
import MenuItem from "./MenuItem/MenuItem";
import {addDish, getDishes} from "../../store/actions/menuActions";
import Spinner from "../UI/Spinner/Spinner";

class Menu extends Component {
	
	componentDidMount() {
		this.props.getDishes();
	};
	
	render() {
		const menu = (
			Object.keys(this.props.dishes).map(dish => {
				return (
					<MenuItem
						key={dish}
						name={this.props.dishes[dish].name}
						price={this.props.dishes[dish].price}
						url={this.props.dishes[dish].url}
						click={() => this.props.onDishAdded(dish)}
					/>
				)
			})
		);
		
		return (
			<div className="menu">
				{this.props.loading ? <Spinner/> : menu}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		dishes: state.dh.dishes,
		loading: state.dh.loading
	}
};

const mapDispatchToProps = dispatch => {
	return {
		onDishAdded: (dishName) => dispatch(addDish(dishName)),
		getDishes: () => dispatch(getDishes())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);