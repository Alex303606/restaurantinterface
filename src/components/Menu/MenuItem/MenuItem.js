import React from 'react'

const MenuItem = props => {
	return (
		<div className="menuItem">
			<div className="image">
				<img src={props.url} alt="dish"/>
			</div>
			<div className="descr">
				<div className="name">{props.name}</div>
				<div className="price">{props.price} KGS</div>
			</div>
			<button onClick={props.click}>Add to cart</button>
		</div>
	)
};

export default MenuItem;