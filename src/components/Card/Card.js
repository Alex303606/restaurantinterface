import React, {Component, Fragment} from 'react';
import './Card.css'
import {connect} from 'react-redux';
import {removeDish} from "../../store/actions/menuActions";
import ItemCard from "./ItemCard/ItemCard";
import Modal from "../UI/Modal/Modal";
import Order from "../Order/Order";
import {purchaseCancelHandler, purchaseHandler} from "../../store/actions/OrderActions";

class Card extends Component {
	
	isPurchasable = () => {
		return this.props.totalPrice > 0;
	};
	
	render() {
		return (
			<Fragment>
				<Modal
					show={this.props.purchasing}
					closed={this.props.purchaseCancelHandler}
				>
					<Order/>
				</Modal>
				
				<div className="card">
					<div className="title">Card</div>
					<ul className="list">
						{
							Object.keys(this.props.dishes).map(dish => {
								if (this.props.dishes[dish].amount > 0) {
									return <ItemCard
										key={dish}
										name={this.props.dishes[dish].name}
										amount={this.props.dishes[dish].amount}
										price={this.props.dishes[dish].price}
										delete={() => this.props.onDishRemoved(dish)}
									/>
								} else {
									return null;
								}
							})
						}
					</ul>
					<div className="bottom-block">
						<div className="delivery">Доставка: <span>{this.props.delivery} KGS</span></div>
						<div
							className="totalPrice">Итого: <span>{this.props.delivery + this.props.totalPrice} KGS</span>
						</div>
						<button onClick={this.props.purchaseHandler} disabled={!this.isPurchasable()}
						        className="place_order">Оформить заказ
						</button>
					</div>
				</div>
			</Fragment>
		)
	}
}

const mapStateToProps = state => {
	return {
		dishes: state.dh.dishes,
		totalPrice: state.dh.totalPrice,
		delivery: state.dh.delivery,
		purchasing: state.cd.purchasing
	}
};

const mapDispatchToProps = dispatch => {
	return {
		onDishRemoved: (dishName) => dispatch(removeDish(dishName)),
		purchaseCancelHandler: () => dispatch(purchaseCancelHandler()),
		purchaseHandler: () => dispatch(purchaseHandler())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(Card);