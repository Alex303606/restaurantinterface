import React from 'react';
import './ItemCard.css';
const ItemCard = props => (
	<li className="ItemCard">
		<span className="name">{props.name}</span>
		<span className="amount">X {props.amount}</span>
		<span className="total">{props.amount * props.price} KGS</span>
		<button onClick={props.delete} className="delete">X</button>
	</li>
);

export default ItemCard;
