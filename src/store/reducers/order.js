import * as actionTypes from '../actions/actionsTypes';

const initialState = {
	loading: false,
	error: null,
	purchasing: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.ORDER_REQUEST:
			return {...state, loading: true, error: null};
		case actionTypes.ORDER_SUCCESS:
			return {...state, loading: false, error: null, purchasing: false};
		case actionTypes.ORDER_ERROR:
			return {...state, loading: false, error: action.error};
		case actionTypes.PURCHASING_TRUE:
			return {...state, purchasing: true};
		case actionTypes.PURCHASING_FALSE:
			return {...state, purchasing: false};
		default:
			return state;
	}
};

export default reducer;