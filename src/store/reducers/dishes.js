import * as actionsTypes from '../actions/actionsTypes';

const initialState = {
	dishes: {},
	delivery: 150,
	totalPrice: 0,
	loading: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionsTypes.ADD_DISH:
			return {
				...state,
				dishes: {
					...state.dishes,
					[action.dishName]: {
						...state.dishes[action.dishName],
						amount: state.dishes[action.dishName].amount + 1
					}
				},
				totalPrice: state.totalPrice + state.dishes[action.dishName].price
			};
		case actionsTypes.REMOVE_DISH:
			return {
				...state,
				dishes: {
					...state.dishes,
					[action.dishName]: {
						...state.dishes[action.dishName],
						amount: 0
					}
				},
				totalPrice: state.totalPrice - (state.dishes[action.dishName].price * state.dishes[action.dishName].amount)
			};
		case actionsTypes.DISHES_REQUEST:
			return {...state, loading: true, error: null};
		case actionsTypes.DISHES_SUCCESS:
			return {...state, loading: false, error: null, dishes: action.dishes};
		case actionsTypes.DISHES_ERROR:
			return {...state, loading: false, error: action.error};
		case actionsTypes.RESET_ORDER_LIST:
			const newDishes = {};
			Object.keys(state.dishes).map(key => {
				return newDishes[key] = {...state.dishes[key], amount: 0};
			});
			return {...state, dishes: newDishes, totalPrice: 0};
		default:
			return state;
	}
	
};

export default reducer;


