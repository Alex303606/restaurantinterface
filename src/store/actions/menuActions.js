import * as actionsTypes from './actionsTypes';
import axios from '../../axios-orders';

export const addDish = dishName => {
	return {type: actionsTypes.ADD_DISH, dishName}
};

export const removeDish = dishName => {
	return {type: actionsTypes.REMOVE_DISH, dishName}
};

export const dishesRequest = () => {
	return {type: actionsTypes.DISHES_REQUEST};
};

export const dishesSuccess = (dishes) => {
	return {type: actionsTypes.DISHES_SUCCESS, dishes};
};

export const dishesError = error => {
	return {type: actionsTypes.DISHES_ERROR, error};
};

export const getDishes = () => {
	return (dispatch) => {
		dispatch(dishesRequest());
		axios.get('/dishes.json').then(response => {
			if (response.data) dispatch(dishesSuccess(response.data));
		}, error => {
			dispatch(dishesError(error));
		});
	}
};

export const resetOrderList = () => {
	return {type: actionsTypes.RESET_ORDER_LIST};
};