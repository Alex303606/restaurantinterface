import * as actionsTypes from './actionsTypes';
import axios from "../../axios-orders";

export const orderRequest = () => {
	return {type: actionsTypes.ORDER_REQUEST};
};

export const orderSuccess = () => {
	return {type: actionsTypes.ORDER_SUCCESS};
};

export const orderError = error => {
	return {type: actionsTypes.ORDER_ERROR, error};
};

export const purchaseHandler = () => {
	return {type: actionsTypes.PURCHASING_TRUE};
};

export const purchaseCancelHandler = () => {
	return {type: actionsTypes.PURCHASING_FALSE};
};

export const placeOrder = order => {
	return (dispatch) => {
		dispatch(orderRequest());
		axios.post('/order.json', order).then(response => {
			dispatch(orderSuccess());
		}, error => {
			dispatch(orderError(error));
		});
	}
};