import axios from 'axios'

const instance = axios.create({
	baseURL: 'https://restaurant-508df.firebaseio.com/'
});

export default instance;