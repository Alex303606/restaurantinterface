import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import restaurantDishes from './store/reducers/dishes';
import restaurantCard from './store/reducers/order';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
	dh: restaurantDishes,
	cd: restaurantCard
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
const app = (<Provider store={store}><App/></Provider>);
ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
